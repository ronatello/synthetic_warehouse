import bpy
from random import randrange
import random
import sys
from math import *
from mathutils import Matrix,Vector
import numpy as np
import random
from mathutils.bvhtree import BVHTree
import math
import sys
import os
sys.path.append(os.path.relpath("./scripts/pillared_warehouse/"))
print(os.getcwd())
from place_racks_and_objects import Place_racks_and_objects
from add_forklift import Add_forklift
from add_CB import Add_CB
from add_fire_ext import Add_fire_ext

###################################################################################################################

# Configure your warehouse here


# The warehouse is divided into 3 sections by the pillars.
# Modelling after a real warehouse, racks are always kept in sets of 2


# Use this variable to define how many sets of 2 racks you want in each section of the warehouse
no_rack_column = 1

# Use this variable to define the density of boxes on each rack [0,1]
prob_of_box = 0.5

# Use this variable to set the width of the corridor between 2 select_set
corridor_width = 7

#Use this variable to define the number of racks along the length of the warehouses
num_rack_y = 10

density_forklift = 0.05

corridor_y = 7

num_section_y = 3
########################################################################################################################

Name = 'Dimi'

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)
print(sys.argv)

y_rack = 5.25
#y_start = -(y_rack * (num_rack_y/2.0 - 1) + y_rack/2.0)
#y_end = -y_start

old_length_y = 94
#new_length_y = y_end-y_start
new_length_y = 5.25*(num_rack_y+2) + (num_rack_y/num_section_y)*corridor_y
y_start = -((new_length_y/2)) + 5
y_end = -y_start
scale_y = new_length_y/old_length_y
print(scale_y)


reqd_len = no_rack_column*3*3 + (no_rack_column-1)*3*corridor_width + 3*corridor_width + 1.5*2*no_rack_column
scale_x = reqd_len/31.5
print(scale_x)

scale_z = 1

bpy.ops.import_scene.fbx( filepath = './objects/warehouses/warehouse_pillared/warehouse_final.fbx' )
#bpy.ops.wm.collada_import(filepath='./objects/warehouses/warehouse_pillared/warehouse_3.dae')

print("original dimensions")
print(bpy.data.objects['wall_window014'].dimensions)

bpy.ops.transform.resize(value=(scale_x,scale_y,math.sqrt(1)))
bpy.data.objects['wall_window014'].location = bpy.context.scene.cursor.location


print("new dimension")
print(bpy.data.objects['wall_window014'].dimensions)

# create light datablock, set attributes
#light_data = bpy.data.lights.new(name="light_2.80", type='POINT')
#light_data.energy = 500
#light_data.color = (1, 0.969472, 0.668894)
#light_data.type = 'AREA'

#for i in range(11):
#    for j in range(3):
#        print(i)
#        # create new object with our light datablock
#        light_object = bpy.data.objects.new(name="light"+str(i+1)+"_"+str(j+1), object_data=light_data)

#        # link light object
#        bpy.context.collection.objects.link(light_object)

#        # make it active
#        bpy.context.view_layer.objects.active = light_object

#        #change location
#        light_object.location = (-10+10*j, -37.1+7.65*i, 5.9)

def append_zero(num):
    return "." + str(num).zfill(3)


all_box_loc = './objects/primitives/boxes_avinash/'
rack_loc = './objects/primitives/Racks/modal.dae'
fire_ext_loc = './objects/primitives/Extinguisher/fire1.dae'
forklift_loc = './objects/primitives/Forklift/final.obj'
CB_loc = './objects/primitives/ConveyerBelt/final.obj'
offset_x = -0.5
z_positions = [0.3, 1.63, 2.98, 4.35]
y_positions = [-2.3, -1.2, 0, 1.5]
extinguisher_coords = [[10.5, 46.7, 2.5], [-10.5, 46.7, 2.5], [-10.5, -44.45, 2.5], [10.5, -44.45, 2.5]]
CB_coords = [[6.236, 1.849, 0], [6.405, 32.5, 0]]
forklift_coords = [[0, 10.78, 0], [0, 42.69, 0]]
box_count = {"BoxA":0, "BoxB":0, "BoxC":0, "BoxD":0, "BoxF":0, "BoxG":0, "BoxH":0, "BoxI":0}

distance_between_racks_x = 1
distance_between_racks_y = 2.7


z = 0

x_coord = []
y_coord = []
mark_coord = []
light_coord=[]
fork_coord=[]


var = (no_rack_column-1)/2

scaling = 12*scale_x

section = 10*scale_x

#def get_coord(factor):

#    x_coord=[]

for i in range(no_rack_column):
    for j in range(3):
        x_coord.append( (-scaling+(corridor_width*i)) + section*j)
        x_coord.append( (-scaling+1.5+(corridor_width*i))+ section*j)

#            return x_coord

#x_coord = get_coord(0)
#mark_coord = get_coord(1)
#light_coord = get_coord(2)

for i in range(no_rack_column):
    for j in range(3):
        mark_coord.append( (-scaling+(corridor_width*i)) -1 + section*j)
        mark_coord.append( (-scaling+1.5+(corridor_width*i)) +1 + section*j)

for i in range(no_rack_column):
    for j in range(3):
        light_coord.append( (-scaling+(corridor_width*i)) -2 + section*j)
        light_coord.append( (-scaling+1.5+(corridor_width*i)) +2 + section*j)

for i in range(no_rack_column):
    for j in range(3):
        fork_coord.append( (-scaling+(corridor_width*i)) -1.5 + section*j)
        fork_coord.append( (-scaling+1.5+(corridor_width*i)) +1.5 + section*j)




y = y_start
while y <= y_end:
    y_coord.append(y)
    y += y_rack

#print(x_coord)
#print(y_coord)

subscript = 0
#x_coord = x_start

# def add_fire_ext(extinguisher_coords):
#
#     subscript = 0
#     model = "fire_ext_10285_Fire_Extinguisher_v3_iterations-2"
#
#     for coords in extinguisher_coords:
#
#         for obj in bpy.data.objects:
#             obj.tag = True
#
#         bpy.ops.import_scene.obj(filepath=fire_ext_loc)
#
#         for obj in bpy.data.objects:
#             if obj.tag is False:
#                 imported_object = obj
#
#         name = imported_object.name
#
#         bpy.data.objects[name].location.x += coords[0]
#         bpy.data.objects[name].location.y += coords[1]
#         bpy.data.objects[name].location.z += coords[2]
#
#         subscript += 1

#fire_ext = Add_fire_ext(extinguisher_coords,fire_ext_loc,subscript)
#fire_ext.add_fire_ext()

#fork_lift = Add_forklift(forklift_coords,forklift_loc)
#fork_lift.add_forklift()

#CB = Add_CB(CB_coords,CB_loc)
#CB.add_CB()

racks_boxes = Place_racks_and_objects(subscript,prob_of_box,rack_loc,all_box_loc,z_positions,y_positions,box_count,offset_x,x_coord,y_coord,z,num_section_y,corridor_y)
racks_boxes.add_racks_boxes()

# for x in mark_coord:
#     for y in y_coord:
#         imported_object = bpy.ops.wm.collada_import(filepath='./objects/primitives/RackMarker/marker.dae')
#         bpy.ops.transform.translate(value=(x, y, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
#
# for x in fork_coord:
#     for y in y_coord:
#         if  random.randint(0,1) <= density_forklift:
#             imported_object = bpy.ops.wm.collada_import(filepath='./objects/primitives/Forklift/final1.dae')
#             bpy.ops.transform.translate(value=(x, y, 0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
#
# light_data = bpy.data.lights.new(name="light_2.80", type='POINT')
# light_data.energy = 500
# light_data.color = (1, 0.969472, 0.668894)
# light_data.type = 'AREA'
#
# for i in light_coord:
#     for j in y_coord:
# #        print(i)
#         # create new object with our light datablock
#         light_object = bpy.data.objects.new(name="light"+str(i+1)+"_"+str(j+1), object_data=light_data)
#
#         # link light object
#         bpy.context.collection.objects.link(light_object)
#
#         # make it active
#         bpy.context.view_layer.objects.active = light_object
#
#         #change location
#         light_object.location = (i, j, 5.9)
# ## update scene, if needed
dg = bpy.context.evaluated_depsgraph_get()
dg.update()
bpy.ops.wm.collada_export(filepath='./rendered_warehouse_scaled/'+'1'+'.dae')
##bpy.ops.export_scene.fbx(filepath='./rendered_warehouse/' +
##gty
