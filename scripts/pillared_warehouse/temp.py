import bpy
from random import randrange
import random
import sys
from math import *
from mathutils import Matrix,Vector
import numpy as np
import random
from mathutils.bvhtree import BVHTree
import math
from scripts.pillared_warehouse.place_racks_and_objects import Place_racks_and_objects
from scripts.pillared_warehouse.add_forklift import Add_forklift
from scripts.pillared_warehouse.add_CB import Add_CB
from scripts.pillared_warehouse.add_fire_ext import Add_fire_ext

no_rack_column = 4

prob_of_box = 0.5
Name = 'Dimi'

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)
print(sys.argv)

num_rack_y = 14  #14
y_rack = 5.25
y_start = -(y_rack * (num_rack_y/2.0 - 1) + y_rack/2.0)
y_end = -y_start

old_length_y = 94
new_length_y = y_end-y_start
scale_y = new_length_y/old_length_y
print(scale_y)

corridor = 9
num_rack_layer_x = 14  # should be 6, 14, 22...
x_rack = 1.5
x_start = -(num_rack_layer_x/4.0 * x_rack + (num_rack_layer_x - 2) * corridor/8.0)
x_end = -x_start

old_length_x = 31.5
new_length_x = x_end-x_start
scale_x = new_length_x/old_length_x
print(scale_x)

scale_z = 1
bpy.ops.import_scene.fbx( filepath = './objects/warehouses/warehouse_pillared/warehouse_final.fbx' )
#bpy.ops.wm.collada_import(filepath='./objects/warehouses/warehouse_pillared/warehouse_3.dae')

print("original dimensions")
print(bpy.data.objects['wall_window014'].dimensions)
print(no_rack_column)
scale_x = no_rack_column/3
corridor_width = 7
reqd_len = no_rack_column*3*3 + (no_rack_column-1)*3*corridor_width + 3*corridor_width
scale_x = reqd_len/29
print(scale_x)
bpy.ops.transform.resize(value=(scale_x,math.sqrt(1),math.sqrt(1)))
bpy.data.objects['wall_window014'].location = bpy.context.scene.cursor.location

print("new dimension")
print(bpy.data.objects['wall_window014'].dimensions)

# create light datablock, set attributes
light_data = bpy.data.lights.new(name="light_2.80", type='POINT')
light_data.energy = 500
light_data.color = (1, 0.969472, 0.668894)
light_data.type = 'AREA'

for i in range(11):
    for j in range(3):
        print(i)
        # create new object with our light datablock
        light_object = bpy.data.objects.new(name="light"+str(i+1)+"_"+str(j+1), object_data=light_data)

        # link light object
        bpy.context.collection.objects.link(light_object)

        # make it active
        bpy.context.view_layer.objects.active = light_object

        #change location
        light_object.location = (-10+10*j, -37.1+7.65*i, 5.9)

def append_zero(num):
    return "." + str(num).zfill(3)


all_box_loc = './objects/primitives/boxes_avinash/'
rack_loc = './objects/primitives/Racks/modal.dae'
fire_ext_loc = './objects/primitives/Extinguisher/fire1.dae'
forklift_loc = './objects/primitives/Forklift/final.obj'
CB_loc = './objects/primitives/ConveyerBelt/final.obj'
offset_x = -0.5
z_positions = [0.3, 1.63, 2.98, 4.35]
y_positions = [-2.3, -1.2, 0, 1.5]
extinguisher_coords = [[10.5, 46.7, 2.5], [-10.5, 46.7, 2.5], [-10.5, -44.45, 2.5], [10.5, -44.45, 2.5]]
CB_coords = [[6.236, 1.849, 0], [6.405, 32.5, 0]]
forklift_coords = [[0, 10.78, 0], [0, 42.69, 0]]
box_count = {"BoxA":0, "BoxB":0, "BoxC":0, "BoxD":0, "BoxF":0, "BoxG":0, "BoxH":0, "BoxI":0}

distance_between_racks_x = 1
distance_between_racks_y = 2.7


#num_rack_y = 14  #14
#y_rack = 5.25
#y_start = -(y_rack * (num_rack_y/2.0 - 1) + y_rack/2.0)
#y_end = -y_start

#corridor = 5
#num_rack_layer_x = 14  # should be 6, 14, 22...
#x_rack = 1.5
#x_start = -(num_rack_layer_x/4.0 * x_rack + (num_rack_layer_x - 2) * corridor/8.0)
#x_end = -x_start

z = 0

x_coord = []
y_coord = []

x = x_start

var = (no_rack_column-1)/2

scaling = 12*scale_x

section = 10*scale_x

for i in range(no_rack_column):
    for j in range(3):
        x_coord.append( (-scaling+(corridor_width*i)) + section*j)
        x_coord.append( (-scaling+1.5+(corridor_width*i)) + section*j)

#x_coord = [-10+10*0,-10+10*1,,-10+10*2]
#while x <= x_end:
#    x_coord.append(x)
#    x += (2 * x_rack + corridor)

y = y_start
while y <= y_end:
    y_coord.append(y)
    y += y_rack

#print(x_coord)
#print(y_coord)

subscript = 0
#x_coord = x_start

# def add_fire_ext(extinguisher_coords):
#
#     subscript = 0
#     model = "fire_ext_10285_Fire_Extinguisher_v3_iterations-2"
#
#     for coords in extinguisher_coords:
#
#         for obj in bpy.data.objects:
#             obj.tag = True
#
#         bpy.ops.import_scene.obj(filepath=fire_ext_loc)
#
#         for obj in bpy.data.objects:
#             if obj.tag is False:
#                 imported_object = obj
#
#         name = imported_object.name
#
#         bpy.data.objects[name].location.x += coords[0]
#         bpy.data.objects[name].location.y += coords[1]
#         bpy.data.objects[name].location.z += coords[2]
#
#         subscript += 1

#fire_ext = Add_fire_ext(extinguisher_coords,fire_ext_loc,subscript)
#fire_ext.add_fire_ext()

#fork_lift = Add_forklift(forklift_coords,forklift_loc)
#fork_lift.add_forklift()

#CB = Add_CB(CB_coords,CB_loc)
#CB.add_CB()

#racks_boxes = Place_racks_and_objects(subscript,prob_of_box,rack_loc,all_box_loc,z_positions,y_positions,box_count,offset_x,x_coord,y_coord,x_rack,z)
#racks_boxes.add_racks_boxes()

def append_zero(num):
    return "." + str(num).zfill(3)

#bpy.ops.wm.collada_import(filepath=rack_loc)
#model = "Rack"
##change = append_zero(subscript)
##if subscript > 0:
##    name = model + change
##else:
##    name = model

#for y in y_coord:
#    for j in range(3):
#        print(i)
#        # create new object with our light datablock
#        model=bpy.ops.wm.collada_import(filepath=rack_loc)
##        model = "Rack"
#        bpy.context.view_layer.objects.active = bpy.data.objects[model]
#        #change location
##        bpy.data.objects[model].location = (-10+10*j, y, 5.9)
#        bpy.data.objects[model].location.x += -10+10*j
#        bpy.data.objects[model].location.y +=  y
#        bpy.data.objects[model].location.z += 0
#imported_object = bpy.ops.wm.collada_import(filepath=rack_loc)
def place_racks_and_objects(rack_location,subscript):
        print("check1")
        imported_object = bpy.ops.wm.collada_import(filepath=rack_loc)
        print("check2")
        model = "Rack"
        change = append_zero(subscript)
        if subscript > 0:
            name = model + change
        else:
            name = model
        bpy.ops.transform.translate(value=(rack_location[0]/2, rack_location[1]/2, rack_location[2]), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
#        bpy.data.objects[model].location.x += rack_location[0]
#        bpy.data.objects[model].location.y += rack_location[1]
#        bpy.data.objects[model].location.z += rack_location[2]
        boxes = ["BoxB","BoxA", "BoxC","BoxH"]
        prev_model=None
        # for rows in self.z_positions:
        #     for cols in self.y_positions:
        #         if  random.randint(0,1) <= self.prob_of_box:
        #             model = random.choice(boxes)
        #             model_temp = model
        # #            model = model+(append_zero(box_count[model_temp]))
        #
        #             change = self.append_zero(self.box_count[model_temp])
        #             if self.box_count[model_temp] > 0:
        #                 model = model + change
        #             else:
        #                 model = model
        #
        #             self.box_count[model_temp] += 1
        #             final_model_location = self.all_box_loc + model_temp + "/model1.dae"
        #             print(final_model_location)
        #             imported_object = bpy.ops.wm.collada_import(
        #                 filepath=final_model_location)
        #             obj = bpy.data.objects[model]
        #             rot_mat = Matrix.Rotation(radians(random.randint(0, 45)), 4, 'Z')
        #             orig_loc, orig_rot, orig_scale = obj.matrix_world.decompose()
        #             print(orig_loc)
        #             orig_loc_mat = Matrix.Translation(orig_loc)
        #             print(Vector((10, 10, 10)))
        #             orig_rot_mat = orig_rot.to_matrix().to_4x4()
        #             orig_scale_mat = np.dot(np.dot(Matrix.Scale(orig_scale[0],4,(1,0,0)),Matrix.Scale(orig_scale[1],4,(0,1,0))),Matrix.Scale(orig_scale[2],4,(0,0,1)))
        #             obj.matrix_world = np.dot(orig_loc_mat,np.dot(rot_mat,np.dot(orig_rot_mat,orig_scale_mat)))
        #             bpy.data.objects[model].location.x = self.offset_x+ rack_location[0]
        #             bpy.data.objects[model].location.y = cols + rack_location[1]
        #             bpy.data.objects[model].location.z = rows + rack_location[2]
        #             prev_model = model

def add_racks_boxes():
    subscript = 0
    for x in x_coord:

        for y in y_coord:
            model = "Rack Model"

            change = append_zero(subscript)
            if subscript > 0:
                name = model + change
            else:
                name = model
#            racks_1 = place_racks_and_objects([x, y, z], subscript,prob_of_box)
#            racks_1.place_racks_and_objects_func()
            place_racks_and_objects([x, y, z], subscript)

            subscript += 1

            change = append_zero(subscript)
            if subscript > 0:
                name = model + change
            else:
                name = model
           # racks_2 = place_racks_and_objects([x + x_rack, y, z], subscript,prob_of_box,rack_loc,all_box_loc,z_positions,y_positions,box_count,offset_x)
           # racks_2.place_racks_and_objects_func()
            # self.place_racks_and_objects([x + self.x_rack, y, self.z], self.subscript)

            subscript += 1

add_racks_boxes()
#bpy.data.objects[model].location.x += -10.65
#bpy.data.objects[model].location.y += 0
#bpy.data.objects[model].location.z += 0

##Deselect all
#bpy.ops.object.select_all(action='DESELECT')

##Mesh objects
#MSH_OBJS = [m for m in bpy.context.scene.objects if m.type == 'MESH']

#for OBJS in MSH_OBJS:
#    #Select all mesh objects
#    OBJS.select_set(state=True)

#    #Makes one active
#    bpy.context.view_layer.objects.active = OBJS

##Joins objects
#bpy.ops.object.join()

#bpy.ops.transform.resize(value=(math.sqrt(scale_x),math.sqrt(scale_y),math.sqrt(scale_z)))

## update scene, if needed
dg = bpy.context.evaluated_depsgraph_get()
dg.update()
bpy.ops.wm.collada_export(filepath='./rendered_warehouse_scaled/'+sys.argv[4]+'.dae')
##bpy.ops.export_scene.fbx(filepath='./rendered_warehouse/' +
##gty
