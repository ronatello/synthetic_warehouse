# Synthetic_warehouse
Synthetic warehouse development, rendering and applications
# Update on 15th July, 2020

* Added 2 new scripts- rackmakerrotation.py and warehousepillarednew.py
* Added 2 new models - Forklift and Conveyer Belt
* Created a new folder boxesavinash - for the modified boxes

# Update on 15th August, 2020

* Modularized the code. Now changes can be made to different objects in the warehouse without affecting other objects.

# How to Generate the warehouse

* Go to ./scripts/pillared_warehouse/warehouse_pillared_new.py and run it. This should generate the warehouse.

* You can make change positions of different objects by going to its respective script in the same folder.

# Update on 27th August, 2020

* User can now enter the number of racks along the length and width of the warehouse. Based on the input, the warehouse will automatically scale itself in the required directions.